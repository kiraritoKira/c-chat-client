import React from 'react'

import { Wrapper } from './styles'

const Loader = () => {
  return (
    <Wrapper>
      <div className="rotating logo">CM</div>
    </Wrapper>
  )
}

export default Loader
