import styled, { keyframes } from 'styled-components'

export const keyFramesRotation = keyframes`
  from {
    transform: rotate(0deg)
  }
  to {
    transform: rotate(360deg)
  }
  `

export const Wrapper = styled.div`
  height: 100vh;
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
  background: #ef0009;
  display: flex;
  align-items: center;
  justify-content: center;
  .logo {
    color: #fff;
    font-size: 30px;
    animation: ${keyFramesRotation} 2s ease-in-out 0s infinite;
  }
`
