import React, { useState, useRef } from 'react'

// Tools
import useOutsideClick from 'hooks/useOutsideClick'

// Styles
import { Wrapper } from './styles'

const Dropdown = props => {
  const { categoryName, items, setConversation } = props

  // Visibility state controller
  const [isOpened, setOpen] = useState(false)

  // Converstaion click handler
  const handleConversationClick = conversation => {
    setOpen(false)
    setConversation(conversation)
  }

  // Outside click controller
  const blockRef = useRef()
  useOutsideClick(blockRef, () => setOpen(false))

  // Arrow UI controller
  const setArrowStyle = () => (isOpened ? 'opened' : '')
  return (
    <Wrapper
      ref={blockRef}
      className={`card category-dropdown ${setArrowStyle()}`}
      onClick={() => setOpen(!isOpened)}
    >
      <div className="category-name">{categoryName}</div>
      {isOpened && (
        <ul className="list">
          {items.map(item => (
            <li
              className="conversation"
              key={item.id}
              onClick={() => handleConversationClick(item)}
            >
              {item.name}
            </li>
          ))}
        </ul>
      )}
    </Wrapper>
  )
}

export default Dropdown
