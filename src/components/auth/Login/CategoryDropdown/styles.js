import styled from 'styled-components'

export const Wrapper = styled.div`
  margin: 0;
  padding: 24px;
  cursor: pointer;
  position: relative;
  &.opened {
    &:after {
      transform: rotate(180deg);
    }
  }
  &:after {
    content: '';
    position: absolute;
    top: 30px;
    right: 10px;
    width: 0;
    height: 0;
    border-left: 6px solid transparent;
    border-right: 6px solid transparent;
    border-top: 6px solid #d3d3d3;
  }
  .category-name {
    text-transform: uppercase;
    font-weight: bold;
    font-size: 16px;
  }
  .list {
    margin-top: 16px;
    text-align: left;
    font-size: 14px;
    .conversation {
      margin: 8px 8px 8px 32px;
      padding: 4px 0;
      transition: all 0.3s;
      &:hover {
        background: #dcdcdc;
      }
    }
  }
`
