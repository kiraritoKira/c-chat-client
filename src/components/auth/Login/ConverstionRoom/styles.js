import styled from 'styled-components'

// Assets
import nextSvg from 'assets/next.svg'

export const Wrapper = styled.div`
  flex: 1;
  .conversation-title {
    font-weight: bold;
    font-size: 16px;
    padding: 16px;
    width: fit-content;
    margin: 16px 0 0 16px;
    background: rgba(255, 255, 255, 0.7);
  }
  .message-form {
    padding: 16px;
    height: 70%;
    position: relative;
    overflow: hidden;
  }
  .message {
    font-size: 16px;
    font-weight: bold;
    padding: 16px;
    width: 40%;
    background: rgba(255, 255, 255, 0.6);
    border-radius: 8px;
    &:not(:last-child) {
      margin-bottom: 16px;
    }
    &.mine {
      margin-left: auto;
      text-align: right;
      .name {
        display: none;
        width: 100px;
      }
    }
    span {
      display: block;
      font-size: 14px;
    }
  }
  .form {
    height: 20%;
    padding: 0 16px;
    position: relative;
    .send-btn {
      cursor: pointer;
      position: absolute;
      top: 0;
      bottom: 0;
      margin: auto 0;
      right: 24px;
      height: 24px;
      width: 24px;
      background-image: url(${nextSvg});
    }
    input {
      outline: none;
      border: none;
      padding: 12px 32px 12px;
      width: 100%;
      box-sizing: border-box;
      background: rgba(255, 255, 255, 0.9);
    }
  }
`
