import React, { useEffect, useState, useRef } from 'react'
import useSocket, { sendMessage } from 'hooks/useSocket'

// Redux
import chatActions from 'redux/chat/actions'
import { connect } from 'react-redux'

// Styles
import { Wrapper } from './styles'

// Components
import Scrollbar from 'react-perfect-scrollbar'

const ConversationRoom = props => {
  const {
    userInfo,
    conversations,
    setConversations,
    currentConversation,
  } = props

  // Scrollbar controller
  const scrollRef = useRef()
  const pushScrollBottom = () => {
    if (scrollRef.current) {
      window.scrollTo(0, scrollRef.current.offsetTop)
    }
  }
  useEffect(() => {
    pushScrollBottom()
  }, [])

  // Input controller
  const [text, setText] = useState('')

  // Send hook
  const handleReceiveMessage = message => {
    let newConversations = conversations
    newConversations[message['convId']].push(message)
    setText('')
    setConversations({ ...newConversations })
    pushScrollBottom()
  }

  // Message controller
  const message = useSocket()
  useEffect(() => {
    if (message) {
      handleReceiveMessage(message)
    }
  }, [message])

  const onSend = e => {
    e.preventDefault()
    const data = {
      text,
      authorId: userInfo.id,
      authorName: userInfo.name,
      conversationId: currentConversation.id,
    }
    sendMessage(data)
  }

  return (
    <Wrapper display="flex_column flex_j">
      {currentConversation && (
        <>
          <div className="conversation-title card">
            {currentConversation.name}
          </div>
          <ul className="message-form">
            <Scrollbar>
              {conversations[currentConversation.id].map(message => {
                return (
                  <li
                    className={`message ${
                      message.authorName === userInfo.name ? ' mine' : ''
                    }`}
                    key={message.id}
                  >
                    <div className="name">{message.authorName}:</div>
                    <span>{message.text}</span>
                  </li>
                )
              })}
            </Scrollbar>
          </ul>
          <form
            onSubmit={onSend}
            className="form flex flex_a_center"
            ref={scrollRef}
          >
            <input
              className="input"
              type="text"
              value={text}
              onChange={e => setText(e.target.value)}
            />
            <div className="send-btn" onClick={onSend} />
          </form>
        </>
      )}
    </Wrapper>
  )
}

const mapStateToProps = state => ({
  conversations: state.chat.conversations,
})

const mapDispatchToProps = dispatch => ({
  setConversations: conversations =>
    dispatch(chatActions.setConversations(conversations)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ConversationRoom)
