import styled from 'styled-components'

// Assets
import image from 'assets/LoginImage.png'

export const Wrapper = styled.div`
  height: 100%;
  overflow: hidden;
  width: 100%;
  background-image: url(${image});
  background-size: cover;
  header {
    margin: 24px 0;
    padding: 32px;
    .block-info {
      margin-left: 16px;
      cursor: pointer;
      justify-content: space-between;
      .t:first-child {
        display: none;
      }
      &:hover {
        .t:first-child {
          display: block;
        }
        .t:last-child {
          text-decoration: line-through;
        }
      }
    }
    .t {
      font-size: 10px;
      text-align: right;
      color: #b2afaf;
      &_username {
        font-size: 14px;
        font-weight: bold;
        color: #494949;
      }
    }
    .avatar {
      border-radius: 50%;
      height: 40px;
      width: 40px;
      color: #fff;
      font-weight: bold;
      background: red;
    }
  }
  .column {
    &_25 {
      padding: 24px;
      width: 25%;
      height: 100%;
      background: rgba(0, 0, 0, 0.1);
    }
  }
  .list {
    border-radius: 8px;
    .list-item {
      width: fit-content;
      padding: 8px 16px;
      &:not(:last-child) {
        margin-bottom: 32px;
      }
    }
  }
  .image {
    flex: 1;
    height: 100%;
    background: url(${image});
    background-size: cover;
  }
`
