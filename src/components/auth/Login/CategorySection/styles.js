import styled from 'styled-components'

export const CategoriesWrapper = styled.div`
  flex: 1;
  padding-right: 8px;
  height: 100%;
  .inputContainer {
    display: block;
    margin-bottom: 16px;
  }
  .title {
    font-size: 16px;
    margin-bottom: 4px;
    text-transform: uppercase;
  }
  .categories-list {
    height: 60%;
    overflow: hidden;
    position: relative;
  }
  .category-dropdown {
    &:not(:last-child) {
      margin-bottom: 16px;
    }
  }
`
