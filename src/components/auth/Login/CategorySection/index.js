import React, { useState, useEffect } from 'react'

// Redux
import { connect } from 'react-redux'
import chatActions from 'redux/chat/actions'

// Components
import CategoryDropdown from '../CategoryDropdown'
import Scrollbar from 'react-perfect-scrollbar'

// Styles
import { CategoriesWrapper } from './styles'

const CategorySection = props => {
  const { categories, getCategies, setConversation } = props

  // Filter controller
  const [inputFilter, setFilter] = useState('')

  // Filter data controller
  const filterSelector = items =>
    items
      ? items.filter(item =>
          item.name.toLowerCase().includes(inputFilter.toLowerCase()),
        )
      : []

  useEffect(() => {
    getCategies()
  }, [])

  return (
    <CategoriesWrapper>
      <label className="inputContainer flex">
        <div className="title">Categories</div>
        <input
          type="text"
          placeholder="Search for categories"
          onChange={e => setFilter(e.target.value)}
          className="input input_filter"
        />
      </label>
      <ul className="categories-list">
        <Scrollbar>
          {filterSelector(categories).map(category => (
            <CategoryDropdown
              key={category.id}
              categoryName={category.name}
              items={category.conversations}
              setConversation={setConversation}
            />
          ))}
        </Scrollbar>
      </ul>
    </CategoriesWrapper>
  )
}

CategorySection.defaultProps = {
  categories: [
    {
      id: '1',
      name: 'fowejfpjw',
    },
    {
      id: '2',
      name: 'fowejfpjw',
    },
    {
      id: '3',
      name: 'fowejfpjw',
    },
  ],
}

const mapStateToProps = state => ({
  categories: state.chat.categories,
})

const mapDispatchToProps = dispatch => ({
  getCategies: () => dispatch(chatActions.getCategories()),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CategorySection)
