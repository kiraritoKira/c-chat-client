import React, { useEffect, useState } from 'react'

// Redux
import { connect } from 'react-redux'
import loginActions from 'redux/auth/actions'

import { loadingSelector } from 'redux/loadings/selectors'
import { getUserInfo } from 'redux/auth/selectors'

// Components
import Loader from 'components/_common/Loader'
import CategorySection from './CategorySection'
import ConversationRoom from './ConverstionRoom'

// Styles
import { Wrapper } from './styles'

const Login = props => {
  const { userInfo, handleLogin, loading } = props

  // Conversation controller
  const [conversation, setConversation] = useState(null)

  useEffect(() => {
    handleLogin()
  }, [])

  // Avatar name generator

  const generateName = name => name.substring(0, 2).toUpperCase()

  return !userInfo ? (
    <Loader />
  ) : (
    <Wrapper className="flex">
      <div className="column_25">
        <header className="card flex flex_a_center">
          <div className="avatar flex flex_a_center flex_j_center">
            {generateName(userInfo.name)}
          </div>
          <div className="block-info">
            <div className="t_username">{userInfo.name}</div>
          </div>
        </header>
        <CategorySection setConversation={setConversation} />
      </div>
      <ConversationRoom
        currentConversation={conversation}
        userInfo={userInfo}
      />
    </Wrapper>
  )
}

const mapStateToProps = state => ({
  userInfo: getUserInfo(state),
  loading: loadingSelector('login', state),
})

const mapDispatchToProps = dispatch => ({
  handleLogin: () => dispatch(loginActions.login()),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login)
