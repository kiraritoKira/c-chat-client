import React from 'react'

// Styles
import styled from 'styled-components'

export const AppWrapper = styled.div`
  margin-top: 60px;
  height: calc(100vh - 60px);
  display: flex;
  width: 100vw;
  background: #fff;
  overflow: hidden;
`
export const HeaderWrapper = styled.header`
  height: 60px;
  width: 100%;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  background: rgb(20, 28, 38);
  box-shadow: 0px 8px 44px -7px rgba(0, 0, 0, 0.75);
`
