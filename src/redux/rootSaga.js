import { all } from 'redux-saga/effects'
import { loginRequest } from './auth/saga'
import { categoriesRequest } from './chat/saga'

export default function* rootSaga() {
  yield all([loginRequest(), categoriesRequest()])
}
