const actions = {
  SET_ERROR: 'SET_ERROR',
  setError: (key, error) => ({
    type: actions.SET_ERROR,
    key,
    error,
  }),
}

export default actions
