import actions from './actions'

const initialState = {}

export default (state = initialState, { type, key, error }) => {
  switch (type) {
    case actions.SET_ERROR:
      return { ...state, [key]: error }
    default:
      return state
  }
}
