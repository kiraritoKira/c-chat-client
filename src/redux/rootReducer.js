import { combineReducers } from 'redux'
import auth from './auth/reducer'
import errors from './errors/reducer'
import loadings from './loadings/reducer'
import chat from './chat/reducer'

export default combineReducers({
  auth,
  errors,
  loadings,
  chat,
})
