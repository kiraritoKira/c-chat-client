import actions from './actions'

const initialState = {
  userInfo: null,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.LOGIN_SUCCESS:
      return { ...state, userInfo: action.userInfo }

    default:
      return state
  }
}
