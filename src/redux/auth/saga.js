import actions from './actions'
import loadingActions from '../loadings/actions'
import errorActions from '../errors/actions'
import { takeEvery, put } from 'redux-saga/effects'

import axios from 'settings/axios'

export function* loginRequest() {
  yield takeEvery(actions.LOGIN_REQUEST, function*(action) {
    yield put(loadingActions.setLoading('login', true))
    try {
      const token = localStorage.getItem('token')
      const { data } = yield axios.post('/login', { token })
      const userInfo = data.data
      yield put(actions.setUserInfo(userInfo))
      localStorage.setItem('token', userInfo.token)
      yield put(loadingActions.setLoading('login', false))
    } catch (e) {
      yield put(loadingActions.setLoading('login', false))
      yield put(errorActions.setError('login', e))
    }
  })
}
