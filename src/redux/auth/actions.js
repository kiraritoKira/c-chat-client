const actions = {
  LOGIN_REQUEST: 'LOGIN_REQUEST',
  LOGIN_SUCCESS: 'LOGIN_SUCCESS',
  login: token => ({
    type: actions.LOGIN_REQUEST,
    token,
  }),
  setUserInfo: userInfo => ({
    type: actions.LOGIN_SUCCESS,
    userInfo,
  }),
}

export default actions
