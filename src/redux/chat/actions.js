const actions = {
  CATEGORIES_REQUEST: 'CATEGORIES_REQUEST',
  CATEGORIES_SUCCESS: 'CATEGORIES_SUCCESS',
  SET_CONVERSATIONS: 'SET_CONVERSATIONS',
  ADD_MESSAGE: 'ADD_MESSAGE',
  getCategories: () => ({
    type: actions.CATEGORIES_REQUEST,
  }),
  setCategories: categories => ({
    type: actions.CATEGORIES_SUCCESS,
    categories,
  }),
  setConversations: conversations => ({
    type: actions.SET_CONVERSATIONS,
    conversations,
  }),
}

export default actions
