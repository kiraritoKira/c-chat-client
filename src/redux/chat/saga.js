import actions from './actions'
import loadingActions from '../loadings/actions'
import errorActions from '../errors/actions'
import { takeEvery, put } from 'redux-saga/effects'

import axios from 'settings/axios'

const generateConversations = categories => {
  let conversations = {}
  for (let i = 0; i < categories.length; i++) {
    for (let j = 0; j < categories[i].conversations.length; j++) {
      conversations[categories[i].conversations[j].id] =
        categories[i].conversations[j].messages
    }
  }
  return conversations
}

export function* categoriesRequest() {
  yield takeEvery(actions.CATEGORIES_REQUEST, function*(action) {
    yield put(loadingActions.setLoading('categories', true))
    try {
      const { data } = yield axios.get('/categories')
      const categories = data.data
      yield put(actions.setCategories(categories))
      yield put(actions.setConversations(generateConversations(categories)))
      yield put(loadingActions.setLoading('categories', false))
    } catch (e) {
      yield put(loadingActions.setLoading('categories', false))
      yield put(errorActions.setError('categories', e))
    }
  })
}
