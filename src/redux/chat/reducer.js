import actions from './actions'

const initialState = {
  categories: null,
  conversations: null,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.CATEGORIES_SUCCESS:
      return { ...state, categories: action.categories }
    case actions.SET_CONVERSATIONS:
      return { ...state, conversations: action.conversations }
    default:
      return state
  }
}
