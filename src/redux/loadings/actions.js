const actions = {
  SET_LOADING: 'SET_LOADING',
  setLoading: (key, loading) => ({
    type: actions.SET_LOADING,
    key,
    loading,
  }),
}

export default actions
