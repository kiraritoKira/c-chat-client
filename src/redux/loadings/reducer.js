import actions from './actions'

const initialState = {
  login: false,
  categories: false,
}

export default (state = initialState, { type, key, loading }) => {
  switch (type) {
    case actions.SET_LOADING:
      return { ...state, [key]: loading }

    default:
      return state
  }
}
