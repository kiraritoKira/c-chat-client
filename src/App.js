import React, { useEffect } from 'react'

import { AppWrapper, HeaderWrapper } from 'layouts/AppLayout'
import MainRouter from 'settings/routing/MainRouter'

const App = props => {
  return (
    <div className="App">
      <AppWrapper>
        <HeaderWrapper />
        <MainRouter />
      </AppWrapper>
    </div>
  )
}

export default App
