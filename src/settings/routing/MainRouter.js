import React, { Suspense } from 'react'

// Components
import Loader from 'components/_common/Loader'
import { Switch, Route } from 'react-router-dom'

// Configs
import routs from './routs.config'

// Tools
import shortid from 'shortid'

const MainRouter = () => (
  <Switch>
    <Suspense fallback={<Loader />}>
      {routs.map(route => (
        <Route
          key={shortid.generate()}
          path={route.path}
          component={route.component}
        />
      ))}
    </Suspense>
  </Switch>
)

export default MainRouter
