import { lazy } from 'react'

const Login = lazy(() => import('components/auth/Login'))

const routs = [
  {
    path: '/',
    component: Login,
  },
]

export default routs
