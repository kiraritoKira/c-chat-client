import { useEffect, useState } from 'react'
import openSocket from 'socket.io-client'

const socket = openSocket('http://192.168.0.105:8000')
const HANDLE_NEW_MESSAGE = 'addNewMessage'
const HANDLE_RECIEVE_NEW_MESSAGE = 'receiveNewMessage'

const useSocket = () => {
  const [message, setMessage] = useState(null)

  socket.on(HANDLE_RECIEVE_NEW_MESSAGE, newMessage => setMessage(newMessage))
  return message
}

export const sendMessage = message => {
  socket.emit(HANDLE_NEW_MESSAGE, message)
}

export default useSocket
